name := "Videri-code-test"

version := "1.0"

organization := "Immanuel"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.8",
  "com.typesafe.akka" %% "akka-slf4j" % "2.4.8",
  "io.spray" %% "spray-http" % "1.3.3",
  "io.spray" %% "spray-can" % "1.3.3",
  "io.spray" %% "spray-routing" % "1.3.3",
  "io.spray" %% "spray-json" % "1.3.1",
  "ch.qos.logback"    %  "logback-classic" % "1.1.2",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "io.spray" %% "spray-testkit" % "1.3.1" % "test"

)

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Spray Repository"    at "http://repo.spray.io")
