
import spray.routing.{HttpService, HttpServiceActor}



class RestInterface extends HttpServiceActor with RestApi{

  def receive = runRoute(route)

}





trait RestApi extends HttpService{
  import ResourceMetricJsonProtocol._
  import PayloadFunctions._

  var entries = 0

  val route =
    path("resourceMetrics") {
      post {
       entity(as[ResourceMetricsRequest]) { response =>

        entries += 1

         val mostFrequent = mostFrequentMap(response)
         val leastFrequent = leastFrequentChar(response)
         val charWordRatio = ratioMostUsedCharPerWord(mostFrequent.maxBy(_._2)._2,wordCount(response))
         val output = new ResourceMetricsResponse(mostFrequent.maxBy(_._2)._1.toString,leastFrequent,charWordRatio)
        complete(output)

       }
          } ~
      get {
        complete(entries + " Post(s) made to the web service")
      }
        }


}


