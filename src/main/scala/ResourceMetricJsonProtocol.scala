import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class ResourceMetricsRequest(id: Long, payload:String, time: Long)
case class ResourceMetricsResponse(most_frequent_char: String,
                                   least_frequent_char:String,
                                   ratio_most_used_char_per_word: Float)

object ResourceMetricJsonProtocol extends DefaultJsonProtocol with SprayJsonSupport{
  implicit val ResourceMetricsRequestFormat = jsonFormat3(ResourceMetricsRequest)
  implicit val ResourceMetricsResponseFormat = jsonFormat3(ResourceMetricsResponse)
}