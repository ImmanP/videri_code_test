
object PayloadFunctions {



   def mostFrequentMap(resourceMetric:ResourceMetricsRequest ): Map[Char,Int] = {

     //create a character list from string (removing whitespace)
    val payload = resourceMetric.payload.replaceAll(" ","")

     //returns a Map with char as the key and count as the value
    payload.groupBy(identity).mapValues(_.size)

  }

  def leastFrequentChar(resourceMetric:ResourceMetricsRequest ): String = {

    //create a character list from string (removing whitespace)
    val payload = resourceMetric.payload.replaceAll(" ","").toList

    //returns a Map with char as the key and count as the value, then returns the char with min value
    payload.groupBy(identity).mapValues(_.size).minBy(_._2)._1.toString

  }

   def wordCount(resourceMetric:ResourceMetricsRequest ): Int = {


     val payload = resourceMetric.payload.split("\\s+")
     payload.size
  }

   def ratioMostUsedCharPerWord(charCount:Int,wordCount:Int): Float = {

     charCount.toFloat / wordCount.toFloat
  }


}
