# Videri Code Test #

### To run the web service ###

Clone the repository:
```> git clone https://ImmanP@bitbucket.org/ImmanP/videri_code_test.git```

Navigate to project folder:

```> cd Videri_code_test```

Run:

```> sbt run```

The web service is running on port 8080 by default. To make a POST request: 

```
>curl -v -H "Content-Type: application/json" \
     -X POST http://localhost:8080/resourceMetrics\
     -d '{"id": 1 , "payload": "the eye", "time": 1458853122409}'

```

To perform a GET request:

```> curl --request GET 'http://localhost:8080/resourceMetrics' ```